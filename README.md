dict_test
=========

``` bash
# Клонируем репозиторий
git clone git@bitbucket.org:Deceased/dict_test.git && cd dict_test

# Устанавливаем зависимости (в процессе указываем необходимые параметры)
composer install

# Обновляем схему БД
php bin/console doctrine:schema:update -- force

# Загружаем демо данные
php bin/console doctrine:fixtures:load

# запускаем приложение
php bin/console server:run
```
