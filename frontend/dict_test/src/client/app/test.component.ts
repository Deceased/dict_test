import { Component, OnInit } from '@angular/core';

import { QuestionsService } from './questions.service';
import { Question } from './models/question';
import { AuthService } from './auth.service';

@Component({
    moduleId: module.id,
    selector: 'dict-test',
    template: `
        <div class="card-content white-text">
            <div *ngIf="question && !showEndScreen">
                <div class="card-title">Выберите правильный перевод</div>
                <h2>{{question.word}}</h2>
                <ul>
                    <li *ngFor="let variant of question.variants">
                        <a class="chip focus"
                           (click)="checkAnswer(variant)"
                        >{{variant}}</a>
                    </li>
                </ul>
                <div *ngIf="message">
                    <div class="card-panel red darken-4">{{ message }}</div>
                </div>
            </div>
            <div *ngIf="showEndScreen">
                <div class="card-title">Тест завершён!!!</div>
                Вы набрали {{ correctAnswers }} {{ getWordForm(correctAnswers, ['балл', 'балла', 'баллов']) }}
            </div>
        </div>

    `,
    providers: [QuestionsService]
})

export class TestComponent implements OnInit {
    question: Question;

    mistakes: number = 0;
    correctAnswers: number = 0;
    showEndScreen: boolean = false;
    message: string;

    constructor(private questionsService: QuestionsService, private authService: AuthService) {
    }

    ngOnInit(): void {
        this.loadQuestion();
    }

    checkAnswer(variant: string): void {
        this.questionsService.checkAnswer(this.question.word, variant).then((correct) => {
            if (!correct) {
                this.mistakes++;
                this.message = 'Не верно!';
                if (this.mistakes >= 3) {
                    this.goToEndScreen();
                }
                return;
            }
            this.message = '';
            this.correctAnswers++;
            this.goToNextQuestion();
        });
    }

    goToNextQuestion(): void {
        this.loadQuestion();
    }

    goToEndScreen(): void {
        this.questionsService.saveResult(this.authService.getName(), this.correctAnswers).then(() => this.showEndScreen = true);
    }

    getWordForm(number: number, forms: string[]): string {
        let form: string;
        let i: number;
        number = number % 100;
        if (number >= 11 && number <= 19) {
            form = forms[2];
        } else {
            i = number % 10;
            switch (i) {
                case (1):
                    form = forms[0];
                    break;
                case (2):
                case (3):
                case (4):
                    form = forms[1];
                    break;
                default:
                    form = forms[2];
            }
        }
        return form;
    }

    private loadQuestion() {
        this.questionsService.getNextQuestion().then((question) => {
            if (!question) {
                this.goToEndScreen();
            }
            this.question = question;
        });
    }
}
