import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Question } from './models/question';

@Injectable()
export class QuestionsService {
    constructor(private http: Http) {}

    getNextQuestion(): Promise<Question> {
        return this.http.get('/random_word')
            .toPromise()
            .then(response => {
                let json = response.json();
                if (json.status === 'finished') {
                    return null;
                }
                return json.data as Question;
            });
    }

    checkAnswer(word: string, answer: string): Promise<boolean> {
        let params: URLSearchParams = new URLSearchParams();
        params.set('word', word);
        params.set('answer', answer);
        return this.http.get('/check_answer', { search: params })
            .toPromise()
            .then(response => {
                return response.json().correct;
            });
    }

    saveResult(name: string, correctAnswers: number): Promise<void> {
        const body = new URLSearchParams();
        body.set('name', name);
        body.set('correctAnswers', '' + correctAnswers);
        return this.http
            .post(
                '/save_result',
                body.toString(),
                {headers: new Headers({'Content-Type': 'application/x-www-form-urlencoded'})})
            .toPromise()
            .then((response) => null);
    }
}
