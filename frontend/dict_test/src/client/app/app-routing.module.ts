import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TestComponent }   from './test.component';
import { AuthComponent }   from './auth.component';

import { CanGoToTestGuard } from './can-go-to-test.guard';

const routes: Routes = [
    { path: '', component: AuthComponent },
    { path: 'test',  component: TestComponent, canActivate: [CanGoToTestGuard] },
];

@NgModule({
    imports: [ RouterModule.forRoot(routes, { useHash: true })],
    exports: [ RouterModule ],
})
export class AppRoutingModule {}
