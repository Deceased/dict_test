import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {
    private name: string = '';

    isLoggedIn(): boolean {
        return this.name.length > 0;
    }

    setName(name:  string): void {
        this.name = name;
    }

    getName(): string {
        return this.name;
    }
}
