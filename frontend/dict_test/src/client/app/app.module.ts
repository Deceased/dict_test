import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent }  from './app.component';
import { TestComponent }  from './test.component';
import { AuthComponent }  from './auth.component';
import { AuthService } from './auth.service';
import { CanGoToTestGuard } from './can-go-to-test.guard';

import { AppRoutingModule } from './app-routing.module';

@NgModule({
  imports:      [ BrowserModule, FormsModule, AppRoutingModule, HttpModule ],
  declarations: [ AppComponent, TestComponent, AuthComponent ],
  providers: [ AuthService, CanGoToTestGuard ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
