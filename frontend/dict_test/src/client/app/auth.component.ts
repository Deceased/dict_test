import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

@Component({
    moduleId: module.id,
    selector: 'auth',
    template: `
        <div class="card-content white-text">
            <div class="card-title">Представьтесь</div>
            <div class="input-field col s12">
                <input [(ngModel)]="name" placeholder="Имя" name="name"/>
            </div>
            <a (click)="auth(name)" class="btn-large">Начать тест</a>
        </div>
    `,
})

export class AuthComponent {
    name: string = '';

    constructor(private authService: AuthService, private router: Router) {
    }

    auth(name: string): void {
        this.authService.setName(name);
        this.router.navigate(['/test/']);
    }
}
