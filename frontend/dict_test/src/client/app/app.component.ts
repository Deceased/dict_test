import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    template:`
        <div class="container">
            <div class="row">
                <h1 class="col s12">{{ name }}</h1>
            </div>
            <div class="row">
                <div class="col s12">
                    <div class="card blue-grey">
                        <router-outlet></router-outlet>
                    </div>
                </div>
            </div>
        </div>
    `,
})

export class AppComponent {
    name = 'Словарь';
}
