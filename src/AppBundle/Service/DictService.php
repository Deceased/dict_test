<?php

namespace AppBundle\Service;

use AppBundle\Entity\Mistake;
use AppBundle\Entity\Word;
use AppBundle\Entity\WordRepository;

class DictService
{
	private $doctrine;

	public function __construct($doctrine)
	{
		$this->doctrine = $doctrine;
	}

	/**
	 * @param string $givenWord
	 * @param string $translation
	 *
	 * @return bool
	 */
	public function checkWordTranslation(string $givenWord, string $translation)
	{
		/** @var WordRepository $repository */
		$repository = $this->doctrine->getRepository('AppBundle:Word');
		/** @var Word $word */
		$word = $repository->findOneByWord($givenWord);
		if (!$word) {
			throw new \RuntimeException("Word not found \"{$givenWord}\"");
		}
		$correct = $word->isTranslationCorrect($translation);
		if (!$correct) {
			$mistake = new Mistake();
			$mistake
				->setQuestion($word->getWord())
				->setAnswer($translation)
				->setDateCreated(new \DateTime());

			$em = $this->doctrine->getManager();
			$em->persist($mistake);
			$em->flush();
		}
		return $correct;
	}
}