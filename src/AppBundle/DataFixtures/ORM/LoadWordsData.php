<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Word;

class LoadWordsData implements FixtureInterface
{
	public function load(ObjectManager $manager)
	{
		foreach (json_decode($this->json, true) as $english => $russian) {
			$this->createWord($english, $russian, "en", "ru", $manager);
			$this->createWord($russian, $english, "ru", "en", $manager);
		}
	}

	private function createWord($text, $translation, $from, $to, $manager)
	{
		$word = new Word();
		$word->setWord($text);
		$word->setTranslation($translation);
		$word->setFrom($from);
		$word->setTo($to);
		$manager->persist($word);
		$manager->flush();
	}

	private $json = '{
		"apple": "яблоко",
		"peach": "персик",
		"orange": "апельсин",
		"grape": "виноград",
		"lemon": "лимон",
		"pineapple": "ананас",
		"watermelon": "арбуз",
		"coconut": "кокос",
		"banana": "банан",
		"pomelo": "помело",
		"strawberry": "клубника",
		"raspberry": "малина",
		"melon": "дыня",
		"apricot": "абрикос",
		"mango": "манго",
		"plum": "слива",
		"pomegranate": "гранат",
		"cherry": "вишня"
	}';
}
