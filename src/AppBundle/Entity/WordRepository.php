<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

class WordRepository extends EntityRepository
{
	public function findRandomWord($usedWords = [])
	{
		$rsm = new ResultSetMapping();
		$rsm->addScalarResult('id', 'id');
		$m = $this->getEntityManager();
		$query = $m
			->createNativeQuery('SELECT id FROM dict_words WHERE id NOT IN (:used_words) ORDER BY RAND() LIMIT 1', $rsm)
			->setParameter('used_words', $usedWords ?: [0]);
		try {
			return $this->find($query->getSingleScalarResult());
		} catch(\Doctrine\ORM\NoResultException $e) {
			return null;
		}
	}

	/**
	 * @param Word $word
	 *
	 * @return Word[]
	 */
	public function findTranslationVariants($word)
	{
		$rsm = new ResultSetMapping();
		$rsm->addEntityResult('AppBundle\Entity\Word', 'w');
		$rsm->addFieldResult('w', 'id', 'id');
		$rsm->addFieldResult('w', 'word', 'word');
		$rsm->addFieldResult('w', 'translation', 'translation');
		$rsm->addFieldResult('w', 'from', 'from');
		$rsm->addFieldResult('w', 'to', 'to');
		$m = $this->getEntityManager();
		$query = $m->createNativeQuery('SELECT r.*
FROM (
       SELECT FLOOR(mm.min_id + (mm.max_id - mm.min_id + 1) * RAND()) AS id
       FROM (
              SELECT MIN(id) AS min_id,
                     MAX(id) AS max_id
              FROM dict_words
            ) AS mm
         JOIN ( SELECT id dummy FROM dict_words WHERE `from` = :lang AND `translation` != :translation LIMIT 11 ) z
     ) AS init
  JOIN  dict_words AS r  ON r.id = init.id WHERE `from` = :lang AND `translation` != :translation
LIMIT 3;', $rsm)->setParameters([
			'lang' => $word->getTo(),
			'translation' => $word->getWord(),
		]);
		$result = $this->fillVariants($query->getResult(), $query, 3);
		$result[] = $this->findOneByWord($word->getTranslation());
		shuffle($result);
		return $result;
	}

	private function fillVariants($result, $query, $num, $maxTries = 3)
	{
		if (count($result) >= $num) {
			return $result;
		}
		for ($i = 0; $i < $maxTries; $i++) {
			$result = $this->addExtra($result, $query, $num);
			if (count($result) >= $num) {
				return $result;
			}
		}
		return $result;
	}

	private function addExtra($result, $query, $num)
	{
		if (count($result) >= $num) {
			return $result;
		}
		$ids = array_map(function ($w) {
			return $w->getId();
		}, $result);
		$extra = array_filter($query->getResult(), function ($w) use ($ids) {
			return !in_array($w->getId(), $ids);
		});
		return array_slice(array_merge($result, $extra), 0, $num);
	}
}
