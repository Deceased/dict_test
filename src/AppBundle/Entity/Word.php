<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="WordRepository")
 * @ORM\Table(name="dict_words", indexes={@ORM\Index(name="word_idx", columns={"word"}), @ORM\Index(name="translation_idx", columns={"translation"})})
 */
class Word
{
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @var int
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $word;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $translation;

	/**
	 * @ORM\Column(name="`from`", type="string", length=6)
	 * @var string
	 */
	private $from;

	/**
	 * @ORM\Column(name="`to`", type="string", length=6)
	 * @var string
	 */
	private $to;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set word
     *
     * @param string $word
     *
     * @return Word
     */
    public function setWord($word)
    {
        $this->word = $word;

        return $this;
    }

    /**
     * Get word
     *
     * @return string
     */
    public function getWord()
    {
        return $this->word;
    }

    /**
     * Set translation
     *
     * @param string $translation
     *
     * @return Word
     */
    public function setTranslation($translation)
    {
        $this->translation = $translation;

        return $this;
    }

    /**
     * Get translation
     *
     * @return string
     */
    public function getTranslation()
    {
        return $this->translation;
    }

    /**
     * Set from
     *
     * @param string $from
     *
     * @return Word
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get from
     *
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set to
     *
     * @param string $to
     *
     * @return Word
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Get to
     *
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }

    public function isTranslationCorrect(string $translation): bool
    {
	    return $this->translation == trim($translation);
    }
}
