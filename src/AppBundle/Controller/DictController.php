<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Result;
use AppBundle\Entity\Word;
use AppBundle\Entity\WordRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DictController extends Controller
{
	public function randomWordAction(Request $request)
	{
		$session = $request->getSession();
		$usedWords = $session->get('used_words');
		$doctrine = $this->getDoctrine();
		$repository = $doctrine->getRepository('AppBundle:Word');
		$word = $repository->findRandomWord($usedWords);
		if (!$word) {
			$response = new JsonResponse([
				'status' => 'finished'
			]);
			$headers = $response->headers;
			$headers->set('Access-Control-Allow-Origin', 'http://localhost:5555');
			return $response;
		}
		$usedWords[] = $word->getId();
		$session->set('used_words', $usedWords);
		$translations = $repository->findTranslationVariants($word);
		$response = new JsonResponse([
			'status' => 'next',
			'data' => [
				'word' => $word->getWord(),
				'variants' => array_map(function($word) {
					return $word->getWord();
				}, $translations),
			],
		]);
		$headers = $response->headers;
		$headers->set('Access-Control-Allow-Origin', 'http://localhost:5555');
		return $response;
	}

	public function checkAnswerAction(Request $request)
	{
		$dict = $this->container->get('app.dict');
		try {
			$response = new JsonResponse([
				'correct' => $dict->checkWordTranslation($request->get('word'), $request->get('answer')),
			]);
		} catch (\RuntimeException $e) {
			throw $this->createNotFoundException();
		}
		$headers = $response->headers;
		$headers->set('Access-Control-Allow-Origin', 'http://localhost:5555');
		return $response;
	}

	public function saveResultAction(Request $request)
	{
		$name = $request->get('name');
		$answers = $request->get('correctAnswers');

		$result = new Result();
		$result->setName($name)
			->setCorrectAnswersNumber($answers)
			->setDateCreated(new \DateTime());

		$em = $this->getDoctrine()->getManager();
		$em->persist($result);
		$em->flush();

		$session = $request->getSession();
		$session->set('used_words', []);

		$response = new JsonResponse([
			'success' => true,
		]);
		$headers = $response->headers;
		$headers->set('Access-Control-Allow-Origin', 'http://localhost:5555');
		return $response;
	}
}
